package ro.ubbcluj.cs.model;

public class JWTUser {
    private String username;

    public JWTUser() {
    }

    public JWTUser(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
