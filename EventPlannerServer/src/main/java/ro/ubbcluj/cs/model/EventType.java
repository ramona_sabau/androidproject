package ro.ubbcluj.cs.model;

public enum EventType {
    SPORT,
    CONFERENCE,
    PARTY,
    RECEPTION,
    WORKSHOP,
    CULTURAL
}
