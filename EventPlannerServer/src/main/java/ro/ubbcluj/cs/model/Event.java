package ro.ubbcluj.cs.model;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Name parameter is requested.")
    @Length(min = 2, max = 50, message = "Name can have at least 2 characters and more than 50.")
    private String name;

    @NotNull(message = "Description parameter is requested.")
    @Length(min = 2, max = 1000, message = "Description can have at least 2 characters and more than 1000.")
    private String description;

    @NotNull(message = "Start date parameter for event is requested.")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate startDate;

    @NotNull(message = "Start time parameter for event is requested.")
    private LocalTime startTime;

    @NotNull(message = "Type of event parameter is requested.")
    @Enumerated(EnumType.STRING)
    private EventType type;

    @NotNull(message = "Author of event parameter is requested.")
    @ManyToOne(fetch = EAGER)
    @JoinColumn
    private User author;

    @ManyToMany(fetch = EAGER)
    @JoinTable(name = "event_user",
            joinColumns = @JoinColumn(name = "event_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<User> users;

    @AssertFalse(message = "The date picked for event is not valid. You cannot create an event in the past.")
    private boolean isDateInThePast() {
        if (this.startDate != null)
            return this.startDate.isBefore(LocalDate.now());
        else
            return false;
    }

    @AssertFalse(message = "The event must be created with at least 2 hours earlier.")
    private boolean isMadeWith2HoursEarlier() {
        if (this.startDate != null && this.startTime != null)
            return this.startDate.isEqual(LocalDate.now()) && this.startTime.isBefore(LocalTime.now().plusHours(2));
        else
            return false;
    }

    public Event() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", startTime=" + startTime +
                ", type=" + type +
                '}';
    }
}
