package ro.ubbcluj.cs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Street parameter is requested.")
    @NotEmpty(message = "Street field is missing.")
    @Length(min = 3, max = 100, message = "Street can have between 3 and 100 characters.")
    private String street;

    @NotNull(message = "Latitude parameter is requested.")
    @DecimalMin(value = "-90", message = "Latitude cannot be less than -90.")
    @DecimalMax(value = "90", message = "Latitude cannot be higher than 90.")
    private Double latitude;

    @NotNull(message = "Longitude parameter is requested.")
    @DecimalMin(value = "-180", message = "Longitude cannot be less than -180.")
    @DecimalMax(value = "180", message = "Longitude cannot be higher than 180.")
    private Double longitude;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(unique = true, nullable = false)
    @JsonIgnore
    private Event event;

    public Address() {
    }

    public Address(String street, Double latitude, Double longitude, Event event) {
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.event = event;
    }

    public Address(String street, double latitude, double longitude) {
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Address(String street, double latitude, double longitude, Event event) {
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.event = event;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return String.format(
                "Address { id: %d, street: '%s', latitude: %f, longitude: %f }",
                id, street, latitude, longitude);
    }
}
