package ro.ubbcluj.cs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ubbcluj.cs.model.Event;

public interface EventRepository extends JpaRepository<Event, Long> {
}
