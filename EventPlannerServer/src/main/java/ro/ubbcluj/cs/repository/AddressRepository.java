package ro.ubbcluj.cs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ubbcluj.cs.model.Address;
import ro.ubbcluj.cs.model.Event;

public interface AddressRepository extends JpaRepository<Address, Long> {

    Address findOneByEvent(Event event);
}
