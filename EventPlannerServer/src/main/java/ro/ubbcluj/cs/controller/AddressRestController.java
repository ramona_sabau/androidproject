package ro.ubbcluj.cs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubbcluj.cs.model.Address;
import ro.ubbcluj.cs.service.address.AddressService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/secured/event/{id}/address")
public class AddressRestController {

    private static final Logger log = LoggerFactory.getLogger(AddressRestController.class);

    @Autowired
    private AddressService addressServiceImpl;

    @RequestMapping(method = RequestMethod.GET)
    public Address findAddressForEvent(@PathVariable long id) {
        if (log.isDebugEnabled()) {
            log.debug("Find address for event with id = " + id + "operation initiated.");
        }
        return addressServiceImpl.findAddressForEvent(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Address changeAddressForEvent(@PathVariable long eventId, @RequestBody @Valid Address address) {
        if (log.isDebugEnabled()) {
            log.debug("Change address for event with id = " + eventId + " operation initiated for " + address);
        }
        return addressServiceImpl.changeAddressForEvent(eventId, address);
    }
}
