package ro.ubbcluj.cs.service.event;

import ro.ubbcluj.cs.model.Event;

import java.util.List;

public interface EventService {
    List<Event> getAllEvents();

    Event getEvent(long eventId);

    Event saveNewEvent(Event event);

    Event updateEvent(Event event);

    void deleteEvent(long eventId);

    void joinEvent(long eventId, long userId);

    void rejectEvent(long eventId, long userId);
}
