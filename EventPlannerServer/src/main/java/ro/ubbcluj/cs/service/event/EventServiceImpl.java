package ro.ubbcluj.cs.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.model.Event;
import ro.ubbcluj.cs.model.User;
import ro.ubbcluj.cs.repository.EventRepository;
import ro.ubbcluj.cs.repository.UserRepository;
import ro.ubbcluj.cs.service.address.AddressServiceImpl;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private static final Logger log = LoggerFactory.getLogger(AddressServiceImpl.class);

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public Event getEvent(long eventId) {
        Event foundEvent = eventRepository.findOne(eventId);

        if (foundEvent == null) {
            String errorMessage = "Event with id = " + eventId + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        return foundEvent;
    }

    @Override
    @Transactional
    public Event saveNewEvent(Event event) {
        User foundUser = checkIfUserExists(event.getAuthor().getId());
        event.setAuthor(foundUser);

        log.info(event + " is saved as a new entity.");
        return eventRepository.save(event);
    }

    @Override
    @Transactional
    public Event updateEvent(Event event) {
        Long eventId = event.getId();

        if (eventId == null) {
            String errorMessage = event + " cannot be updated because the identifier is not provided.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        Event foundEvent = checkIfEventExists(eventId);

        Long authorId = event.getAuthor().getId();
        if (!foundEvent.getAuthor().getId().equals(authorId)) {
            String errorMessage = event + " cannot be updated because the user that made the request is not the author " +
                    "of the event.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        foundEvent.setName(event.getName());
        foundEvent.setDescription(event.getDescription());
        foundEvent.setStartDate(event.getStartDate());
        foundEvent.setStartTime(event.getStartTime());
        foundEvent.setType(event.getType());

        log.info(event + " is updated.");
        return eventRepository.save(foundEvent);
    }

    @Override
    @Transactional
    public void deleteEvent(long eventId) {
        Event foundEvent = getEvent(eventId);
        eventRepository.delete(foundEvent);
    }

    @Override
    @Transactional
    public void joinEvent(long eventId, long userId) {
        Event eventFound = checkIfEventExists(eventId);
        User userFound = checkIfUserExists(userId);

        //add user to participants list if it is not include yet
        List<User> participants = eventFound.getUsers();
        for (User participant : participants) {
            if (participant.equals(userFound)){
                String errorMessage = userFound + " has already joined the event.";
                log.warn(errorMessage);
                throw new IllegalArgumentException(errorMessage);
            }
        }
        participants.add(userFound);
        eventFound.setUsers(participants);

        //save changes to event
        eventRepository.saveAndFlush(eventFound);
    }

    @Override
    @Transactional
    public void rejectEvent(long eventId, long userId) {
        Event eventFound = checkIfEventExists(eventId);
        User userFound = checkIfUserExists(userId);

        boolean hasListChanges = false;
        //remove user to participants list if it is included
        List<User> participants = eventFound.getUsers();
        List<User> newParticipants = new ArrayList<>();
        for (User participant : participants) {
            if (participant.getId().equals(userFound.getId())){
                hasListChanges = true;
            } else {
                newParticipants.add(participant);
            }
        }

        if (!hasListChanges) {
            String errorMessage = userFound + " has not joined the event. The user cannot be deleted from participants list.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        eventFound.setUsers(newParticipants);

        //save changes to event
        eventRepository.saveAndFlush(eventFound);
    }

    private User checkIfUserExists(Long id) {
        User userFound = userRepository.findOne(id);

        if (userFound == null) {
            String errorMessage = "User with id = " + id + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        return userFound;
    }

    private Event checkIfEventExists(Long id) {
        Event eventFound = eventRepository.findOne(id);

        if (eventFound == null) {
            String errorMessage = "Event with id = " + id + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        return eventFound;
    }
}
