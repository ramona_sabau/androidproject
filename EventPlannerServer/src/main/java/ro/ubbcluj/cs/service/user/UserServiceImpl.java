package ro.ubbcluj.cs.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.model.User;
import ro.ubbcluj.cs.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public User findUserByUsername(String username) {
        User foundUser = userRepository.findOneByUsername(username);

        if (foundUser == null) {
            log.warn("User with username = " + username + " could not be found.");
            return null;
        }

        return foundUser;
    }

    @Override
    public boolean authenticateUser(String username, String password) {
        User user = findUserByUsername(username);

        if (user != null)
        {
            return user.getPassword().equals(password);
        }

        return false;
    }
}
