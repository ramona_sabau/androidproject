package ro.ubbcluj.cs.service.address;

import ro.ubbcluj.cs.model.Address;

public interface AddressService {
    Address findAddressForEvent(Long id);

    Address changeAddressForEvent(Long eventId, Address address);
}
