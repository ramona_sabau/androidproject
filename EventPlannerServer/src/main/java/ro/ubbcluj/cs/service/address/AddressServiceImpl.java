package ro.ubbcluj.cs.service.address;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.model.Address;
import ro.ubbcluj.cs.model.Event;
import ro.ubbcluj.cs.repository.AddressRepository;
import ro.ubbcluj.cs.repository.EventRepository;

@Service
public class AddressServiceImpl implements AddressService {

    private static final Logger log = LoggerFactory.getLogger(AddressServiceImpl.class);

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Address findAddressForEvent(Long id) {
        Event event = checkIfEventExists(id);
        return addressRepository.findOneByEvent(event);
    }

    @Override
    public Address changeAddressForEvent(Long eventId, Address address) {
        Event event = checkIfEventExists(eventId);
        Address existingAddress = addressRepository.findOneByEvent(event);
        if (existingAddress == null) {
            //address.setEvent(event);
            addressRepository.saveAndFlush(address);
            return address;
        } else {
            existingAddress.setStreet(address.getStreet());
            existingAddress.setLatitude(address.getLatitude());
            existingAddress.setLongitude(address.getLongitude());
            addressRepository.saveAndFlush(existingAddress);
            return existingAddress;
        }
    }

    private Event checkIfEventExists(Long id) throws IllegalArgumentException {
        Event event;
        event = eventRepository.findOne(id);
        if (event == null) {
            String errorMessage = "Event with id = " + id + " does not exist.";
            log.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
        return event;
    }
}
