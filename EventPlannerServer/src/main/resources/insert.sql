USE EventPlanner;

INSERT INTO user (username, password, first_name, last_name, email) VALUES
  ("user1", "password1", "User1", "User1", "user1@yahoo.com"),
  ("user2", "password2", "User2", "User2", "user2@yahoo.com"),
  ("user3", "password3", "User3", "User3", "user3@yahoo.com"),
  ("user4", "password4", "User4", "User4", "user4@yahoo.com"),
  ("user5", "password5", "User5", "User5", "user5@yahoo.com"),
  ("user6", "password6", "User6", "User6", "user6@yahoo.com"),
  ("user7", "password7", "User7", "User7", "user7@yahoo.com"),
  ("user8", "password8", "User8", "User8", "user8@yahoo.com"),
  ("user9", "password9", "User9", "User9", "user9@yahoo.com"),
  ("user10", "password10", "User10", "User10", "user10@yahoo.com");

INSERT INTO event (name, description, type, start_date, start_time, author_id) VALUES
  ("Event1", "This is event 1. You are all invited!", "SPORT", current_date + 1, "14:00", 1),
  ("Event2", "This is event 2. You are all invited!", "CONFERENCE", current_date + 1, "15:25", 1),
  ("Event3", "This is event 3. You are all invited!", "SPORT", current_date + 1, "17:38", 2),
  ("Event4", "This is event 4. You are all invited!", "SPORT", current_date + 1, "19:30", 3),
  ("Event5", "This is event 5. You are all invited!", "SPORT", current_date + 1, "22:00", 4);


INSERT INTO event_user(user_id, event_id) VALUES
  (1, 1),
  (1, 2),
  (1, 4),
  (2, 2),
  (3, 2),
  (4, 2),
  (5, 2);