package ro.ubbcluj.cs.eventplannerclient.content;

public enum EventType {
    SPORT,
    CONFERENCE,
    PARTY,
    RECEPTION,
    WORKSHOP,
    CULTURAL
}
