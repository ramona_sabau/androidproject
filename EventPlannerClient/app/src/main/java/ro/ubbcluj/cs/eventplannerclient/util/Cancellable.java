package ro.ubbcluj.cs.eventplannerclient.util;

public interface Cancellable {
  void cancel();
}
