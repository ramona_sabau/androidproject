package ro.ubbcluj.cs.eventplannerclient.util;

public interface OnSuccessListener<E> {
    void onSuccess(E e);
}
