package ro.ubbcluj.cs.eventplannerclient.net.mapping.reader;

import android.media.session.MediaSession;
import android.util.JsonReader;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;

import ro.ubbcluj.cs.eventplannerclient.content.AuthenticatedMember;
import ro.ubbcluj.cs.eventplannerclient.content.User;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.AuthenticatedMember.TOKEN;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.AuthenticatedMember.USER;

public class AuthenticatedMemberReader implements ResourceReader<AuthenticatedMember, JsonReader> {
    private static final String TAG = AuthenticatedMemberReader.class.getSimpleName();

    @Override
    public AuthenticatedMember read(JsonReader jsonReader) throws IOException, JSONException, Exception {
        AuthenticatedMember authenticatedMember = new AuthenticatedMember();

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case(TOKEN):
                    authenticatedMember.setToken(jsonReader.nextString());
                    break;
                case(USER):
                    ResourceReader<User, JsonReader> userResourceReader = new UserReader();
                    authenticatedMember.setUser(userResourceReader.read(jsonReader));
                    break;
                default:
                    jsonReader.skipValue();
                    Log.w(TAG, String.format("AuthenticatedMember property '%s' is ignored.", name));
                    break;
            }
        }
        jsonReader.endObject();

        return authenticatedMember;
    }
}
