package ro.ubbcluj.cs.eventplannerclient.content;

public class Address {
    private long mId;
    private String mStreet;
    private double mLatitude;
    private double mLongitude;
    private Event mEvent;

    public Address() {
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        this.mStreet = street;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    public Event getEvent() {
        return mEvent;
    }

    public void setEvent(Event event) {
        this.mEvent = event;
    }

    @Override
    public String toString() {
        return "Address{" +
                "mId=" + mId +
                ", mStreet='" + mStreet + '\'' +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                ", mEvent=" + mEvent +
                '}';
    }
}
