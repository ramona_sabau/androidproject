package ro.ubbcluj.cs.eventplannerclient.net.mapping.reader;

import android.util.JsonReader;

import java.io.IOException;

import ro.ubbcluj.cs.eventplannerclient.net.Issue;

public class IssueReader implements ResourceReader<Issue, JsonReader> {
  @Override
  public Issue read(JsonReader reader) throws IOException {
    Issue issue = new Issue();
    reader.beginObject();
    while (reader.hasNext()) {
      String name = reader.nextName();
      issue.add(name, reader.nextString());
    }
    reader.endObject();
    return issue;
  }
}
