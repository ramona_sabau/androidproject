package ro.ubbcluj.cs.eventplannerclient.net.mapping.reader;

import org.json.JSONException;

import java.io.IOException;

public interface ResourceReader<E, Reader> {
    E read(Reader reader) throws IOException, JSONException, Exception;
}
