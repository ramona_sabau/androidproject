package ro.ubbcluj.cs.eventplannerclient.net.mapping.writer;

import android.util.JsonWriter;

import java.io.IOException;

import ro.ubbcluj.cs.eventplannerclient.content.PossibleMember;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.LogInRequest.PASSWORD;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.LogInRequest.USERNAME;

public class PossibleMemberWriter implements ResourceWriter<PossibleMember, JsonWriter> {
  @Override
  public void write(PossibleMember possibleMember, JsonWriter writer) throws IOException {
      writer.beginObject();
      writer.name(USERNAME).value(possibleMember.getUsername());
      writer.name(PASSWORD).value(possibleMember.getPassword());
      writer.endObject();
  }
}