package ro.ubbcluj.cs.eventplannerclient;

import android.app.Application;
import android.util.Log;

import ro.ubbcluj.cs.eventplannerclient.net.EventRestClient;
import ro.ubbcluj.cs.eventplannerclient.service.EventManager;

public class EventPlannerClient extends Application {
    private static final String TAG = EventPlannerClient.class.getSimpleName();

    private EventManager mEventManager;
    private EventRestClient mEventRestClient;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        mEventManager = new EventManager(this);
        mEventRestClient = new EventRestClient(this);
        mEventManager.setEventRestClient(mEventRestClient);
    }

    @Override
    public void onTerminate() {
        Log.d(TAG, "onTerminate");
        super.onTerminate();
    }

    public EventManager getEventManager() {
        return mEventManager;
    }
}
