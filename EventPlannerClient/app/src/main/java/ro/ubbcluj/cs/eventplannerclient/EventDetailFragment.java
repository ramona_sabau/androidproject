package ro.ubbcluj.cs.eventplannerclient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.service.EventManager;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.DialogUtils;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

/**
 * A fragment representing a single Event detail screen.
 * This fragment is either contained in a {@link EventListActivity}
 * in two-pane mode (on tablets) or a {@link EventDetailActivity}
 * on handsets.
 */
public class EventDetailFragment extends Fragment {
    private static final String TAG = EventDetailFragment.class.getSimpleName();

    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String EVENT_ID = "event_id";

    /**
     * The event content this fragment is presenting.
     */
    private Event mEvent;

    private EventPlannerClient mApplication;
    private Cancellable mFetchNoteAsync;
    private EventManager mEventManager;

    private TextView mDescriptionTextView;
    private TextView mDateTextView;
    private TextView mTypeTextView;
    private TextView mAuthorTextView;
    private ProgressDialog mProgressDialog;
    private CollapsingToolbarLayout mApplicationBarLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EventDetailFragment() {
    }

    public Event getEvent() {
        return mEvent;
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mApplication = (EventPlannerClient) context.getApplicationContext();
        mEventManager = mApplication.getEventManager();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(EVENT_ID)) {
            // In a real-world scenario, use a Loader
            // to load content from a content provider.
            Activity activity = this.getActivity();
            mApplicationBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        View rootView = inflater.inflate(R.layout.event_detail, container, false);
        mDescriptionTextView = (TextView) rootView.findViewById(R.id.event_description);
        mTypeTextView = (TextView) rootView.findViewById(R.id.event_type);
        mDateTextView = (TextView) rootView.findViewById(R.id.event_date);
        mAuthorTextView = (TextView) rootView.findViewById(R.id.event_author);
        createProgressDialog();
        fetchNoteAsync();

        return rootView;
    }

    private void createProgressDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading event details");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        if (mFetchNoteAsync != null) {
            mFetchNoteAsync.cancel();
        }
    }

    private void fetchNoteAsync() {
        mProgressDialog.show();
        mFetchNoteAsync = mApplication.getEventManager().getEventAsync(
                getArguments().getString(EVENT_ID),
                new OnSuccessListener<Event>() {

                    @Override
                    public void onSuccess(final Event event) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mEvent = event;
                                if (mEvent.getAuthor().getId().equals(mEventManager.getAuthenticatedMember().getUser().getId())) {
                                    Log.d(TAG, "Delete and edit operations can be performed.");

                                    getActivity().findViewById(R.id.fab_delete).setVisibility(View.VISIBLE);
                                    getActivity().findViewById(R.id.fab_edit).setVisibility(View.VISIBLE);
                                }
                                fillNoteDetails();
                            }
                        });
                    }
                }, new OnErrorListener() {

                    @Override
                    public void onError(final Exception e) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.showError(getActivity(), e);
                                mProgressDialog.dismiss();
                            }
                        });
                    }
                });
    }

    private void fillNoteDetails() {
        if (mEvent != null) {
            if (mApplicationBarLayout != null) {
                mApplicationBarLayout.setTitle(mEvent.getName());
            }
            mDescriptionTextView.setText(mEvent.getDescription());
            mTypeTextView.setText(mEvent.getType().toString());
            mDateTextView.setText(mEvent.getStartDate() + " " + mEvent.getStartTime());
            mAuthorTextView.setText(mEvent.getAuthor().getFirstName() + " " + mEvent.getAuthor().getLastName());
        }
        mProgressDialog.dismiss();
    }
}
