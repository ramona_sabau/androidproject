package ro.ubbcluj.cs.eventplannerclient.content;

public class AuthenticatedMember {
    private String mToken;
    private User mUser;

    public AuthenticatedMember() {
    }

    public AuthenticatedMember(String token, User user) {
        this.mToken = token;
        this.mUser = user;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        this.mUser = user;
    }
}
