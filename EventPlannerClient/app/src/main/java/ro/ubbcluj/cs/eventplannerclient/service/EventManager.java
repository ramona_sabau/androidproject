package ro.ubbcluj.cs.eventplannerclient.service;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import ro.ubbcluj.cs.eventplannerclient.content.AuthenticatedMember;
import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.content.PossibleMember;
import ro.ubbcluj.cs.eventplannerclient.net.EventRestClient;
import ro.ubbcluj.cs.eventplannerclient.net.LastModifiedList;
import ro.ubbcluj.cs.eventplannerclient.net.ResourceException;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

public class EventManager extends Observable {
    private static final String TAG = EventManager.class.getSimpleName();

    private final Context mContext;

    private EventRestClient mEventRestClient;

    private ConcurrentMap<Long, Event> mEvents = new ConcurrentHashMap<>();

    private String mEventsLastUpdate;

    private AuthenticatedMember mAuthenticatedMember;

    public EventManager(Context context) {
        mContext = context;
    }

    public void setEventRestClient(EventRestClient eventRestClient) {
        this.mEventRestClient = eventRestClient;
    }

    public AuthenticatedMember getAuthenticatedMember() {
        return mAuthenticatedMember;
    }

    public void setAuthenticatedMember(AuthenticatedMember authenticatedMember) {
        this.mAuthenticatedMember = authenticatedMember;
        this.mEventRestClient.setAuthenticatedMember(authenticatedMember);
    }

    public Cancellable loginAsync(
            String username, String password,
            final OnSuccessListener<AuthenticatedMember> successListener,
            final OnErrorListener errorListener) {
        final PossibleMember possibleMember = new PossibleMember();
        possibleMember.setUsername(username);
        possibleMember.setPassword(password);
        return mEventRestClient.getToken(
                possibleMember, new OnSuccessListener<AuthenticatedMember>() {

                @Override
                public void onSuccess(AuthenticatedMember authenticatedMember) {
                    if (authenticatedMember != null) {
                        setAuthenticatedMember(authenticatedMember);
                        successListener.onSuccess(authenticatedMember);
                    } else {
                        errorListener.onError(new ResourceException(new IllegalArgumentException("Log in operation failed!")));
                    }
                }
            }, errorListener);
    }

    public Cancellable getEventsAsync(final OnSuccessListener<List<Event>> onSuccessListener,
                                      OnErrorListener onErrorListener) {
        Log.d(TAG, "getEventsAsync...");
        return mEventRestClient.searchAsync(mEventsLastUpdate, new OnSuccessListener<LastModifiedList<Event>>() {

            @Override
            public void onSuccess(LastModifiedList<Event> result) {
                Log.d(TAG, "getEventsAsync succeeded");
                List<Event> events = result.getList();
                if (events != null) {
                    mEventsLastUpdate = result.getLastModified();
                    updateCachedEvents(events);
                }
                onSuccessListener.onSuccess(cachedEventsByUpdated());
                notifyObservers();
            }
        }, onErrorListener);
    }

    private void updateCachedEvents(List<Event> events) {
        Log.d(TAG, "updateCachedEvents");
        mEvents = new ConcurrentHashMap<>();
        for (Event event : events) {
            mEvents.put(event.getId(), event);
        }
        setChanged();
    }

    private List<Event> cachedEventsByUpdated() {
        ArrayList<Event> notes = new ArrayList<>(mEvents.values());
        Collections.sort(notes, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                if (o1.getId().equals(o2.getId()))
                    return 0;
                else {
                    return o1.getId().compareTo(o2.getId());
                }
            }
        });
        return notes;
    }

    public Cancellable getEventAsync(final String eventId,
                                     final OnSuccessListener<Event> onSuccessListener,
                                     OnErrorListener onErrorListener) {
        Log.d(TAG, "getEventAsync...");
        return mEventRestClient.readAsync(eventId, new OnSuccessListener<Event>() {

            @Override
            public void onSuccess(Event event) {
                Log.d(TAG, "getEventAsync succeeded");

                Long id = Long.parseLong(eventId);
                if (event == null) {
                    setChanged();
                    mEvents.remove(id);
                } else {
                    if (!event.equals(mEvents.get(event.getId()))) {
                        setChanged();
                        mEvents.put(id, event);
                    }
                }
                onSuccessListener.onSuccess(event);
                notifyObservers();
            }
        }, onErrorListener);
    }

    public Cancellable saveNewEventAsync(final Event event,
                                         final OnSuccessListener<Event> onSuccessListener,
                                         final OnErrorListener onErrorListener) {
        Log.d(TAG, "saveNewEventAsync...");
        return mEventRestClient.saveAsync(
                event, new OnSuccessListener<Event>() {

                    @Override
                    public void onSuccess(Event savedEvent) {
                        Log.d(TAG, "saveNewEventAsync succeeded");
                        if (savedEvent != null) {
                            mEvents.put(savedEvent.getId(), savedEvent);
                            onSuccessListener.onSuccess(savedEvent);
                            setChanged();
                            notifyObservers();
                        } else {
                            onErrorListener.onError(new ResourceException(new IllegalArgumentException("Event could not be saved!")));
                        }
                    }
                }, onErrorListener);
    }

    public Cancellable updateEventAsync(final Event event,
                                         final OnSuccessListener<Event> onSuccessListener,
                                         final OnErrorListener onErrorListener) {
        Log.d(TAG, "updateEventAsync...");
        return mEventRestClient.updateAsync(
                event, new OnSuccessListener<Event>() {

                    @Override
                    public void onSuccess(Event updatedEvent) {
                        Log.d(TAG, "updateEventAsync succeeded");
                        if (updatedEvent != null) {
                            mEvents.put(updatedEvent.getId(), updatedEvent);
                            onSuccessListener.onSuccess(updatedEvent);
                            setChanged();
                            notifyObservers();
                        } else {
                            onErrorListener.onError(new ResourceException(new IllegalArgumentException("Event could not be updated!")));
                        }
                    }
                }, onErrorListener);
    }

    public Cancellable deleteEventAsync(final Long eventId,
                                     final OnSuccessListener<Event> onSuccessListener,
                                     OnErrorListener onErrorListener) {
        Log.d(TAG, "deleteEventAsync...");
        return mEventRestClient.deleteAsync(eventId, new OnSuccessListener<Event>() {

            @Override
            public void onSuccess(Event event) {
                Log.d(TAG, "deleteEventAsync succeeded");

                mEvents.remove(eventId);
                setChanged();

                onSuccessListener.onSuccess(event);
                notifyObservers();
            }
        }, onErrorListener);
    }

}
