package ro.ubbcluj.cs.eventplannerclient;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import ro.ubbcluj.cs.eventplannerclient.content.AuthenticatedMember;
import ro.ubbcluj.cs.eventplannerclient.content.User;
import ro.ubbcluj.cs.eventplannerclient.service.EventManager;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.DialogUtils;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

public class LogInActivity extends AppCompatActivity {
    private static final String TAG = LogInActivity.class.getSimpleName();

    private Cancellable mCancellable;
    private EventManager mEventManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_log_in);
        mEventManager = ((EventPlannerClient) getApplication()).getEventManager();
        //get authenticated user from eventManager if exists
        AuthenticatedMember member = mEventManager.getAuthenticatedMember();
        if (member != null) {
            startEventListActivity();
            finish();
        }
        setupToolbar();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mCancellable != null) {
            mCancellable.cancel();
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
                Snackbar.make(view, "Authentication in progress. Please wait...", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Action", null).show();
            }
        });
    }

    private void login() {
        EditText usernameEditText = (EditText) findViewById(R.id.username);
        EditText passwordEditText = (EditText) findViewById(R.id.password);
        mCancellable = mEventManager
            .loginAsync(
                    usernameEditText.getText().toString(), passwordEditText.getText().toString(),
                    new OnSuccessListener<AuthenticatedMember>() {
                        @Override
                        public void onSuccess(final AuthenticatedMember authenticatedMember) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    startEventListActivity();
                                }
                            });
                        }
                    }, new OnErrorListener() {
                        @Override
                        public void onError(final Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DialogUtils.showError(LogInActivity.this, e);
                                }
                            });
                        }
                    });
    }

    private void startEventListActivity() {
        startActivity(new Intent(this, EventListActivity.class));
    }
}
