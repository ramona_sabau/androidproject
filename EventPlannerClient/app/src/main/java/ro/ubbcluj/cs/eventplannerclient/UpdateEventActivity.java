package ro.ubbcluj.cs.eventplannerclient;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.content.EventType;
import ro.ubbcluj.cs.eventplannerclient.service.EventManager;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.DialogUtils;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.DESCRIPTION;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.ID;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_DATE;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_TIME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.TYPE;

public class UpdateEventActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = UpdateEventActivity.class.getSimpleName();

    private Cancellable mUpdateEventAsyncCall;
    private EventManager mEventManager;
    private Event mEvent;

    private Button mDatePickerButton, mTimePickerButton, mOkButton;
    private TextView mDateTextView, mTimeTextView;
    private EditText mNameEditText, mDescriptionEditText;
    private Spinner mTypeSpinner;
    private ProgressDialog mProgressDialog;
    private ImageView mImageView;
    private Animation mRotateAnimation;
    private Dialog mSuccessDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_right, R.anim.fade_out);
        setContentView(R.layout.activity_create_event);
        mEventManager = ((EventPlannerClient) getApplication()).getEventManager();

        mNameEditText = (EditText) findViewById(R.id.event_name);
        mDescriptionEditText = (EditText) findViewById(R.id.event_description);
        mDateTextView = (TextView) findViewById(R.id.event_date);
        mTimeTextView = (TextView) findViewById(R.id.event_time);
        mTypeSpinner = (Spinner)findViewById(R.id.event_types);

        setupEventTypes();
        loadData(getIntent().getExtras());
        setupToolbar();
        setUpDateAndTimePickers();
        setUpProgressDialog();
        setUpSuccessScreen();
    }

    private void loadData(Bundle bundle) {
        mEvent = new Event();

        mEvent.setId(bundle.getLong(ID));

        mEvent.setName(bundle.getString(NAME));
        mNameEditText.setText(mEvent.getName());

        mEvent.setDescription(bundle.getString(DESCRIPTION));
        mDescriptionEditText.setText(mEvent.getDescription());

        mEvent.setStartDate(bundle.getString(START_DATE));
        mDateTextView.setText(mEvent.getStartDate());

        mEvent.setStartTime(bundle.getString(START_TIME));
        mTimeTextView.setText(mEvent.getStartTime());

        mEvent.setType((EventType) bundle.get(TYPE));
        mTypeSpinner.setSelection(((ArrayAdapter<EventType>) mTypeSpinner.getAdapter()).getPosition(mEvent.getType()));
    }

    private void setupEventTypes() {
        Spinner spinner = (Spinner) findViewById(R.id.event_types);
        ArrayAdapter<EventType> adapter = new ArrayAdapter<EventType>(this,
                android.R.layout.simple_spinner_dropdown_item,
                EventType.values());
        spinner.setAdapter(adapter);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvent();
            }
        });

    }

    private void setUpProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Updating event");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
    }

    private void setUpSuccessScreen() {
        mSuccessDialog = new Dialog(this);
        mSuccessDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mSuccessDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog, null));
        mImageView = (ImageView) mSuccessDialog.findViewById(R.id.success);
        mRotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        mOkButton = (Button) mSuccessDialog.findViewById(R.id.ok_button);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mSuccessDialog.dismiss();
                goBackToListActivity();
            }
        });
    }

    private void goBackToListActivity() {
        Intent i = new Intent(this, EventListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void updateEvent() {
        mProgressDialog.show();
        Event event = new Event();
        event.setId(mEvent.getId());
        event.setName(mNameEditText.getText().toString());
        event.setDescription(mDescriptionEditText.getText().toString());
        event.setStartDate(mDateTextView.getText().toString());
        event.setStartTime(mTimeTextView.getText().toString());
        event.setType(EventType.valueOf(mTypeSpinner.getSelectedItem().toString()));
        event.setAuthor(mEventManager.getAuthenticatedMember().getUser());
        mUpdateEventAsyncCall = mEventManager
                .updateEventAsync(
                        event,
                        new OnSuccessListener<Event>() {
                            @Override
                            public void onSuccess(final Event savedEvent) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d(TAG, "updateEventAsync finished successfully");
                                        mProgressDialog.dismiss();
                                        mSuccessDialog.show();
                                        mImageView.startAnimation(mRotateAnimation);
                                    }
                                });
                            }
                        }, new OnErrorListener() {
                            @Override
                            public void onError(final Exception e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d(TAG, "updateEventAsync finished with errors");
                                        mProgressDialog.dismiss();
                                        DialogUtils.showError(UpdateEventActivity.this, e);
                                    }
                                });
                            }
                        });
    }

    private void setUpDateAndTimePickers() {
        mDatePickerButton = (Button) findViewById(R.id.btn_date);
        mTimePickerButton = (Button) findViewById(R.id.btn_time);

        mDatePickerButton.setOnClickListener(this);
        mTimePickerButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mDatePickerButton) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mDateTextView.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == mTimePickerButton) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            mTimeTextView.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();

        if (mUpdateEventAsyncCall != null) {
            mUpdateEventAsyncCall.cancel();
        }
    }
}
