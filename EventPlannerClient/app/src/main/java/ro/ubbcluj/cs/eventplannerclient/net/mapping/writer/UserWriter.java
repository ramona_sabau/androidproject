package ro.ubbcluj.cs.eventplannerclient.net.mapping.writer;

import android.util.JsonWriter;

import java.io.IOException;

import ro.ubbcluj.cs.eventplannerclient.content.User;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.EMAIL;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.FIRST_NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.ID;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.LAST_NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.USERNAME;

public class UserWriter implements ResourceWriter<User, JsonWriter> {
    @Override
    public void write(User user, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginObject();

        if (user.getId() != null) {
            jsonWriter.name(ID).value(user.getId());
        }

        if (user.getUsername() != null) {
            jsonWriter.name(USERNAME).value(user.getUsername());
        }

        if (user.getFirstName() != null) {
            jsonWriter.name(FIRST_NAME).value(user.getFirstName());
        }

        if (user.getLastName() != null) {
            jsonWriter.name(LAST_NAME).value(user.getLastName());
        }

        if (user.getEmail() != null) {
            jsonWriter.name(EMAIL).value(user.getEmail());
        }

        jsonWriter.endObject();
    }
}
