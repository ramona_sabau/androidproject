package ro.ubbcluj.cs.eventplannerclient.net.mapping.writer;

import android.util.JsonWriter;

import java.io.IOException;
import java.util.List;

import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.content.User;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.AUTHOR;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.DESCRIPTION;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_DATE;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_TIME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.TYPE;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.USERS;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.ID;

public class EventWriter implements ResourceWriter<Event, JsonWriter> {
    @Override
    public void write(Event event, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginObject();

        if (event.getId() != null) {
            jsonWriter.name(ID).value(event.getId());
        }

        if (event.getName() != null) {
            jsonWriter.name(NAME).value(event.getName());
        }

        if (event.getDescription() != null) {
            jsonWriter.name(DESCRIPTION).value(event.getDescription());
        }

        if (event.getType() != null) {
            jsonWriter.name(TYPE).value(event.getType().toString());
        }

        if (event.getStartDate() != null) {
            String[] elements = event.getStartDate().split("-");
            if (elements.length == 3) {
                jsonWriter.name(START_DATE)
                        .beginArray()
                        .value(Integer.parseInt(elements[0]))
                        .value(Integer.parseInt(elements[1]))
                        .value(Integer.parseInt(elements[2]))
                        .endArray();
            }
        }

        if (event.getStartTime() != null) {
            String[] elements = event.getStartTime().split(":");
            if (elements.length == 2) {
                jsonWriter.name(START_TIME)
                        .beginArray()
                        .value(Integer.parseInt(elements[0]))
                        .value(Integer.parseInt(elements[1]))
                        .endArray();
            }
        }

        if (event.getAuthor() != null) {
            jsonWriter.name(AUTHOR);
            ResourceWriter<User, JsonWriter> userWriter = new UserWriter();
            userWriter.write(event.getAuthor(), jsonWriter);
        }

        if (event.getUsers() != null) {
            jsonWriter.name(USERS);
            ResourceWriter<List<User>, JsonWriter> usersWriter = new ResourceListWriter<>(new UserWriter());
            usersWriter.write(event.getUsers(), jsonWriter);
        }

        jsonWriter.endObject();
    }
}
