package ro.ubbcluj.cs.eventplannerclient.content;

public class PossibleMember {
    private String mUsername;
    private String mPassword;

    public PossibleMember() {
    }

    public PossibleMember(String username, String password) {
        this.mUsername = username;
        this.mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    @Override
    public String toString() {
        return "PossibleMember{" +
                "username='" + mUsername + '\'' +
                ", password='" + mPassword + '\'' +
                '}';
    }
}
