package ro.ubbcluj.cs.eventplannerclient.net.mapping.reader;

import android.util.JsonReader;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;

import ro.ubbcluj.cs.eventplannerclient.content.User;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.EMAIL;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.FIRST_NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.ID;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.LAST_NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.User.USERNAME;

public class UserReader implements ResourceReader<User, JsonReader> {
    private static final String TAG = UserReader.class.getSimpleName();

    @Override
    public User read(JsonReader jsonReader) throws IOException, JSONException, Exception {
        User user = new User();

        //begin reading json object
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case(ID):
                    user.setId(jsonReader.nextLong());
                    break;
                case(USERNAME):
                    user.setUsername(jsonReader.nextString());
                    break;
                case(FIRST_NAME):
                    user.setFirstName(jsonReader.nextString());
                    break;
                case(LAST_NAME):
                    user.setLastName(jsonReader.nextString());
                    break;
                case(EMAIL):
                    user.setEmail(jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    Log.w(TAG, String.format("User property '%s' is ignored.", name));
                    break;
            }
        }
        jsonReader.endObject();
        return user;
    }
}
