package ro.ubbcluj.cs.eventplannerclient;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.content.EventType;
import ro.ubbcluj.cs.eventplannerclient.service.EventManager;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.DialogUtils;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

public class CreateEventActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = CreateEventActivity.class.getSimpleName();

    private Cancellable mSaveEventAsyncCall;
    private EventManager mEventManager;

    private Button mDatePickerButton, mTimePickerButton, mOkButton;
    private TextView mDateTextView, mTimeTextView;
    private ImageView mImageView;
    private Animation mRotateAnimation;
    private Dialog mSuccessDialog;
    private ProgressDialog mProgressDialog;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_right, R.anim.fade_out);
        setContentView(R.layout.activity_create_event);
        mEventManager = ((EventPlannerClient) getApplication()).getEventManager();
        setupEventTypes();
        setupToolbar();
        setUpDateAndTimePickers();
        setUpSuccessScreen();
        setUpProgressDialog();
    }

    private void setUpSuccessScreen() {
        mSuccessDialog = new Dialog(this);
        mSuccessDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mSuccessDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog, null));
        mImageView = (ImageView) mSuccessDialog.findViewById(R.id.success);
        mRotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        mOkButton = (Button) mSuccessDialog.findViewById(R.id.ok_button);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mSuccessDialog.dismiss();
                goBackToListActivity();
            }
        });
    }

    private void goBackToListActivity() {
        Intent i = new Intent(this, EventListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void setupEventTypes() {
        Spinner spinner = (Spinner) findViewById(R.id.event_types);
        ArrayAdapter<EventType> adapter = new ArrayAdapter<EventType>(this,
                android.R.layout.simple_spinner_dropdown_item,
                EventType.values());
        spinner.setAdapter(adapter);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNewEvent();
            }
        });

    }

    private void saveNewEvent() {
        mProgressDialog.show();
        EditText nameEditText = (EditText) findViewById(R.id.event_name);
        EditText descriptionEditText = (EditText) findViewById(R.id.event_description);
        TextView startDateTextView = (TextView) findViewById(R.id.event_date);
        TextView startTimeTextView = (TextView) findViewById(R.id.event_time);
        Spinner typeSpinner = (Spinner)findViewById(R.id.event_types);

        Event event = new Event();
        event.setName(nameEditText.getText().toString());
        event.setDescription(descriptionEditText.getText().toString());
        event.setStartDate(startDateTextView.getText().toString());
        event.setStartTime(startTimeTextView.getText().toString());
        event.setType(EventType.valueOf(typeSpinner.getSelectedItem().toString()));
        event.setAuthor(mEventManager.getAuthenticatedMember().getUser());
        mSaveEventAsyncCall = mEventManager
                .saveNewEventAsync(
                        event,
                        new OnSuccessListener<Event>() {
                            @Override
                            public void onSuccess(final Event savedEvent) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mProgressDialog.dismiss();
                                        mSuccessDialog.show();
                                        mImageView.startAnimation(mRotateAnimation);
                                    }
                                });
                            }
                        }, new OnErrorListener() {
                            @Override
                            public void onError(final Exception e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mProgressDialog.dismiss();
                                        DialogUtils.showError(CreateEventActivity.this, e);
                                    }
                                });
                            }
                        });

    }

    private void setUpDateAndTimePickers() {
        mDatePickerButton = (Button) findViewById(R.id.btn_date);
        mTimePickerButton = (Button) findViewById(R.id.btn_time);
        mDateTextView = (TextView) findViewById(R.id.event_date);
        mTimeTextView = (TextView) findViewById(R.id.event_time);

        mDatePickerButton.setOnClickListener(this);
        mTimePickerButton.setOnClickListener(this);
    }

    private void setUpProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Saving new event");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
    }

    @Override
    public void onClick(View v) {
        if (v == mDatePickerButton) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mDateTextView.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == mTimePickerButton) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            mTimeTextView.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();

        if (mSaveEventAsyncCall != null) {
            mSaveEventAsyncCall.cancel();
        }
    }
}


