package ro.ubbcluj.cs.eventplannerclient;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;

import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.service.EventManager;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.DialogUtils;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.AUTHOR;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.DESCRIPTION;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.ID;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_DATE;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_TIME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.TYPE;

/**
 * An activity representing a single Event detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link EventListActivity}.
 */
public class EventDetailActivity extends AppCompatActivity {

    private static final String TAG = EventDetailActivity.class.getSimpleName();

    private EventDetailFragment mEventDetailFragment;

    private EventPlannerClient mApplication;
    private EventManager mEventManager;
    private Cancellable mDeleteEventAsync;

    private ProgressDialog mProgressDialog;
    private ImageView mImageView;
    private Animation mRotateAnimation;
    private Dialog mSuccessDialog;
    private Button mOkButton;

    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    Log.d(TAG, "Delete operation started! User clicked YES.");
                    mProgressDialog.show();
                    mDeleteEventAsync = mEventManager
                            .deleteEventAsync(
                                    mEventDetailFragment.getEvent().getId(),
                                    new OnSuccessListener<Event>() {
                                        @Override
                                        public void onSuccess(final Event savedEvent) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.d(TAG, "deleteEventAsync finished successfully.");
                                                    mProgressDialog.dismiss();
                                                    mSuccessDialog.show();
                                                    mImageView.startAnimation(mRotateAnimation);
                                                }
                                            });
                                        }
                                    }, new OnErrorListener() {
                                        @Override
                                        public void onError(final Exception e) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.d(TAG, "deleteEventAsync finished with errors.");
                                                    mProgressDialog.dismiss();
                                                    DialogUtils.showError(EventDetailActivity.this, e);
                                                }
                                            });
                                        }
                                    });
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    Log.d(TAG, "Delete operation aborted! User clicked NO.");
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_right, R.anim.fade_out);
        setContentView(R.layout.activity_event_detail);
        mApplication = (EventPlannerClient) getApplication();
        mEventManager = mApplication.getEventManager();
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(EventDetailFragment.EVENT_ID,
                    getIntent().getStringExtra(EventDetailFragment.EVENT_ID));
            mEventDetailFragment = new EventDetailFragment();
            mEventDetailFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.event_detail_container, mEventDetailFragment)
                    .commit();
        }

        setUpEditFloatingButton();
        setUpDeleteFloatingButton();
        setUpProgressDialog();
        setUpSuccessScreen();
    }

    private void setUpSuccessScreen() {
        mSuccessDialog = new Dialog(this);
        mSuccessDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mSuccessDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog, null));
        mImageView = (ImageView) mSuccessDialog.findViewById(R.id.success);
        mRotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        mOkButton = (Button) mSuccessDialog.findViewById(R.id.ok_button);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mSuccessDialog.dismiss();
                goBackToListActivity();
            }
        });
    }

    private void goBackToListActivity() {
        Intent i = new Intent(this, EventListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void setUpDeleteFloatingButton() {
        FloatingActionButton fab_delete = (FloatingActionButton) findViewById(R.id.fab_delete);
        fab_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EventDetailActivity.this);
                builder.setMessage("Are you sure you want to delete this event?")
                        .setPositiveButton("Yes", mDialogClickListener)
                        .setNegativeButton("No", mDialogClickListener)
                        .show();
            }
        });
    }

    private void setUpEditFloatingButton() {
        FloatingActionButton fab_edit = (FloatingActionButton) findViewById(R.id.fab_edit);
        fab_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEventDetailFragment != null) {
                    Event event = mEventDetailFragment.getEvent();

                    if (event == null) {
                        Log.d(TAG, "No data is provided to update event.");
                    } else {
                        Log.d(TAG, "Start UpdateEventActivity with current event");
                        startActivity(buildIntent(event));
                    }

                }
            }
        });
    }

    private void setUpProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Delete event in progress");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
    }

    private Intent buildIntent(Event event) {
        Intent i = new Intent(this, UpdateEventActivity.class);
        i.putExtra(ID, event.getId());
        i.putExtra(NAME, event.getName());
        i.putExtra(DESCRIPTION, event.getDescription());
        i.putExtra(START_DATE, event.getStartDate());
        i.putExtra(START_TIME, event.getStartTime());
        i.putExtra(TYPE, event.getType());
        i.putExtra(AUTHOR, event.getAuthor().getFirstName() + " " + event.getAuthor().getLastName());
        return i;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, EventListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mDeleteEventAsync != null) {
            mDeleteEventAsync.cancel();
        }
    }
}
