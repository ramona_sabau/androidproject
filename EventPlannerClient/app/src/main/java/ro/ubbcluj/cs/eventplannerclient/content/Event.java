package ro.ubbcluj.cs.eventplannerclient.content;

import java.util.List;

public class Event {
    private Long mId;
    private String mName;
    private String mDescription;
    private String mStartDate;
    private String mStartTime;
    private EventType mType;
    private User mAuthor;
    private List<User> mUsers;

    public Event() {
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        this.mStartDate = startDate;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String startTime) {
        this.mStartTime = startTime;
    }

    public EventType getType() {
        return mType;
    }

    public void setType(EventType type) {
        this.mType = type;
    }

    public User getAuthor() {
        return mAuthor;
    }

    public void setAuthor(User author) {
        this.mAuthor = author;
    }

    public List<User> getUsers() {
        return mUsers;
    }

    public void setUsers(List<User> users) {
        this.mUsers = users;
    }

    @Override
    public String toString() {
        return "Event{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mStartDate='" + mStartDate + '\'' +
                ", mStartTime='" + mStartTime + '\'' +
                ", mType=" + mType +
                ", mAuthor=" + mAuthor +
                ", mUsers=" + mUsers +
                '}';
    }
}
