package ro.ubbcluj.cs.eventplannerclient.net;

import android.content.Context;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import ro.ubbcluj.cs.eventplannerclient.content.AuthenticatedMember;
import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.content.PossibleMember;
import ro.ubbcluj.cs.eventplannerclient.content.User;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.reader.AuthenticatedMemberReader;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.reader.EventReader;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.reader.IssueReader;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.reader.ResourceListReader;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.reader.ResourceReader;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.writer.EventWriter;
import ro.ubbcluj.cs.eventplannerclient.net.mapping.writer.PossibleMemberWriter;
import ro.ubbcluj.cs.eventplannerclient.util.Cancellable;
import ro.ubbcluj.cs.eventplannerclient.util.CancellableCallable;
import ro.ubbcluj.cs.eventplannerclient.util.OnErrorListener;
import ro.ubbcluj.cs.eventplannerclient.util.OnSuccessListener;

public class EventRestClient {
    private static final String TAG = EventRestClient.class.getSimpleName();
    private static final String APPLICATION_JSON = "application/json";
    private static final String UTF_8 = "UTF-8";
    private static final String LAST_MODIFIED_HEADER = "Last-Modified";
    private static final String AUTHENTICATION_HEADER = "x-auth-token";
    private static final String API_URL = "http://192.168.43.60:8080";
    private static final String EVENT_URL = API_URL + "/api/secured/event";
    private static final String LOG_IN_URL = API_URL + "/api/public/login";

    private final OkHttpClient mOkHttpClient;
    private final Context mContext;
    private AuthenticatedMember mAuthenticatedMember;

    public EventRestClient(Context context) {
        mContext = context;
        mOkHttpClient = new OkHttpClient();
        Log.d(TAG, "NoteRestClient created");
    }

    public void setAuthenticatedMember(AuthenticatedMember authenticatedMember) {
        this.mAuthenticatedMember = authenticatedMember;
    }

    public CancellableOkHttpAsync<AuthenticatedMember> getToken(PossibleMember possibleMember,
                                                                OnSuccessListener<AuthenticatedMember> successListener,
                                                                OnErrorListener errorListener) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new PossibleMemberWriter().write(possibleMember, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, "getToken operation failed", e);
            throw new ResourceException(e);
        }
        return new CancellableOkHttpAsync<AuthenticatedMember>(
                new Request.Builder()
                        .url(LOG_IN_URL)
                        .post(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()))
                        .build(),
                new ResponseReader<AuthenticatedMember>() {
                    @Override
                    public AuthenticatedMember read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        AuthenticatedMember member;
                        switch(response.code()) {
                            case(200):
                                member = new AuthenticatedMemberReader().read(reader);
                                break;
                            case (400):
                                Issue issue = new IssueReader().read(reader);
                                throw new ResourceException(issue);
                            default:
                                member = null;
                                break;
                        }
                        return member;
                    }
                },
                successListener,
                errorListener
        );
    }

    private void addAuthenticationToken(Request.Builder requestBuilder) {
        if (mAuthenticatedMember != null) {
            requestBuilder.header(AUTHENTICATION_HEADER, mAuthenticatedMember.getToken());
        }
    }

    public Cancellable searchAsync(String mEventsLastUpdate,
                                   OnSuccessListener<LastModifiedList<Event>> onSuccessListener,
                                   OnErrorListener onErrorListener) {

        Request.Builder requestBuilder = new Request.Builder().url(EVENT_URL);
        if (mEventsLastUpdate != null) {
            //convert date to UTC
            SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            try {
                Date localDate = dateFormat.parse(mEventsLastUpdate);
                String dateFormatInUTC = dateFormat.format(localDate);
                requestBuilder.header(LAST_MODIFIED_HEADER, dateFormatInUTC);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        addAuthenticationToken(requestBuilder);
        return new CancellableOkHttpAsync<LastModifiedList<Event>>(
                requestBuilder.build(),
                new ResponseReader<LastModifiedList<Event>>() {
                    @Override
                    public LastModifiedList<Event> read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        LastModifiedList<Event> lastModifiedList;
                        switch(response.code()) {
                            case(200):
                                lastModifiedList = new LastModifiedList<Event>(response.header(LAST_MODIFIED_HEADER),
                                        new ResourceListReader<Event>(new EventReader()).read(reader));
                                break;
                            case(304):
                                lastModifiedList = new LastModifiedList<Event>(response.header(LAST_MODIFIED_HEADER),
                                        null);
                                break;
                            case(400):
                                Issue issue = new IssueReader().read(reader);
                                throw new ResourceException(issue);
                            default:
                                lastModifiedList = null;
                                break;
                        }
                        return lastModifiedList;
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    public Cancellable readAsync(String eventId,
                                 OnSuccessListener<Event> onSuccessListener,
                                 OnErrorListener onErrorListener) {
        Request.Builder builder = new Request.Builder().url(String.format("%s/%s", EVENT_URL, eventId));
        addAuthenticationToken(builder);
        return new CancellableOkHttpAsync<Event>(
            builder.build(),
            new ResponseReader<Event>() {
                @Override
                public Event read(Response response) throws Exception {
                    Event event;
                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                    switch(response.code()) {
                        case(200):
                            event = new EventReader().read(reader);
                        break;
                        case (400):
                            Issue issue = new IssueReader().read(reader);
                            throw new ResourceException(issue);
                        default:
                            event = null;
                            break;
                    }
                    return event;
                }
            },
            onSuccessListener,
            onErrorListener
        );
    }

    public Cancellable saveAsync(Event event,
                                   OnSuccessListener<Event> onSuccessListener,
                                   OnErrorListener onErrorListener) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new EventWriter().write(event, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, " updateAsync operation failed", e);
            throw new ResourceException(e);
        }

        Request.Builder requestBuilder = new Request.Builder()
                .url(EVENT_URL)
                .post(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()));
        addAuthenticationToken(requestBuilder);

        return new CancellableOkHttpAsync<Event>(
                requestBuilder.build(),
                new ResponseReader<Event>() {
                    @Override
                    public Event read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        Event savedEvent;
                        switch(response.code()) {
                            case(200):
                                savedEvent = new EventReader().read(reader);
                                break;
                            case (400):
                                Issue issue = new IssueReader().read(reader);
                                throw new ResourceException(issue);
                            default:
                                savedEvent = null;
                                break;
                        }
                        return savedEvent;
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    public Cancellable updateAsync(Event event,
                                   OnSuccessListener<Event> onSuccessListener,
                                   OnErrorListener onErrorListener) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new EventWriter().write(event, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, " updateAsync operation failed", e);
            throw new ResourceException(e);
        }

        Request.Builder requestBuilder = new Request.Builder()
                .url(EVENT_URL)
                .put(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()));
        addAuthenticationToken(requestBuilder);

        return new CancellableOkHttpAsync<Event>(
                requestBuilder.build(),
                new ResponseReader<Event>() {
                    @Override
                    public Event read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        Event savedEvent;
                        switch(response.code()) {
                            case(200):
                                savedEvent = new EventReader().read(reader);
                                break;
                            case (400):
                                Issue issue = new IssueReader().read(reader);
                                throw new ResourceException(issue);
                            default:
                                savedEvent = null;
                                break;
                        }
                        return savedEvent;
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    public Cancellable deleteAsync(final Long eventId,
                                   OnSuccessListener<Event> onSuccessListener,
                                   OnErrorListener onErrorListener) {
        Request.Builder builder = new Request.Builder()
                .url(String.format("%s/%s", EVENT_URL, eventId))
                .delete();
        addAuthenticationToken(builder);
        return new CancellableOkHttpAsync<Event>(
                builder.build(),
                new ResponseReader<Event>() {
                    @Override
                    public Event read(Response response) throws Exception {
                        Event event;
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        switch(response.code()) {
                            case(200):
                                event = new Event();
                                event.setId(eventId);
                                break;
                            case (400):
                                Issue issue = new IssueReader().read(reader);
                                throw new ResourceException(issue);
                            default:
                                event = null;
                                break;
                        }
                        return event;
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    private class OkHttpCancellableCallable<E> implements CancellableCallable<E> {
        private final Call mCall;
        private final Request mRequest;
        private final ResponseReader<E> mResponseReader;

        public OkHttpCancellableCallable(Request request, ResponseReader<E> responseReader) {
            mRequest = request;
            mResponseReader = responseReader;
            mCall = mOkHttpClient.newCall(request);
        }

        @Override
        public E call() throws Exception {
            try {
                Log.d(TAG, String.format("started %s %s", mRequest.method(), mRequest.url()));
                Response response = mCall.execute();
                Log.d(TAG, String.format("succeeded %s %s", mRequest.method(), mRequest.url()));
                if (mCall.isCanceled()) {
                    return null;
                }
                return mResponseReader.read(response);
            } catch (Exception e) {
                Log.e(TAG, String.format("failed %s %s", mRequest.method(), mRequest.url()), e);
                throw e instanceof ResourceException ? e : new ResourceException(e);
            }
        }

        @Override
        public void cancel() {
            if (mCall != null) {
                mCall.cancel();
            }
        }
    }

    private static interface ResponseReader<E> {
        E read(Response response) throws Exception;
    }

    private class CancellableOkHttpAsync<E> implements Cancellable {
        private Call mCall;

        public CancellableOkHttpAsync(
                final Request request,
                final ResponseReader<E> responseReader,
                final OnSuccessListener<E> successListener,
                final OnErrorListener errorListener) {
            try {
                mCall = mOkHttpClient.newCall(request);
                Log.d(TAG, String.format("started %s %s", request.method(), request.url()));
                //retry 3x, renew token
                mCall.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        notifyFailure(e, request, errorListener);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            notifySuccess(response, request, successListener, responseReader);
                        } catch (Exception e) {
                            notifyFailure(e, request, errorListener);
                        }
                    }
                });
            } catch (Exception e) {
                notifyFailure(e, request, errorListener);
            }
        }

        @Override
        public void cancel() {
            if (mCall != null) {
                mCall.cancel();
            }
        }

        private void notifySuccess(Response response, Request request,
                                   OnSuccessListener<E> successListener, ResponseReader<E> responseReader) throws Exception {
            if (mCall.isCanceled()) {
                Log.d(TAG, String.format("completed, but cancelled %s %s", request.method(), request.url()));
            } else {
                Log.d(TAG, String.format("completed %s %s", request.method(), request.url()));
                successListener.onSuccess(responseReader.read(response));
            }
        }

        private void notifyFailure(Exception e, Request request, OnErrorListener errorListener) {
            if (mCall.isCanceled()) {
                Log.d(TAG, String.format("failed, but cancelled %s %s", request.method(), request.url()));
            } else {
                Log.e(TAG, String.format("failed %s %s", request.method(), request.url()), e);
                errorListener.onError(e instanceof ResourceException ? e : new ResourceException(e));
            }
        }
    }
}
