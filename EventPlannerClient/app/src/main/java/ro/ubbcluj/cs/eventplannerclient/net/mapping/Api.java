package ro.ubbcluj.cs.eventplannerclient.net.mapping;

public class Api {
    public static class Event {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String START_DATE = "startDate";
        public static final String START_TIME = "startTime";
        public static final String TYPE = "type";
        public static final String AUTHOR = "author";
        public static final String USERS = "users";
    }

    public static class LogInRequest {
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
    }

    public static class AuthenticatedMember {
        public static final String TOKEN = "token";
        public static final String USER = "user";
    }

    public static class User {
        public static final String ID = "id";
        public static final String USERNAME = "username";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String EMAIL = "email";
    }
}
