package ro.ubbcluj.cs.eventplannerclient.util;

public interface OnErrorListener {
    void onError(Exception e);
}
