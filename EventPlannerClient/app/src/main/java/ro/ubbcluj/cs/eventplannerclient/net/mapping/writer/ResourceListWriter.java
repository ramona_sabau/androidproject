package ro.ubbcluj.cs.eventplannerclient.net.mapping.writer;

import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.IOException;
import java.util.List;

public class ResourceListWriter<E> implements ResourceWriter<List<E>, JsonWriter> {

    private final ResourceWriter<E, JsonWriter> mResourceWriter;

    public ResourceListWriter(ResourceWriter<E, JsonWriter> resourceWriter) {
        mResourceWriter = resourceWriter;
    }

    @Override
    public void write(List<E> list, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginArray();

        for (E entity : list) {
            mResourceWriter.write(entity, jsonWriter);
        }

        jsonWriter.endArray();
    }
}