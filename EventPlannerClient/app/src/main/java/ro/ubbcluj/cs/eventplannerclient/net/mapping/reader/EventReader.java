package ro.ubbcluj.cs.eventplannerclient.net.mapping.reader;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import android.util.StringBuilderPrinter;

import org.json.JSONException;

import java.io.IOException;
import java.security.cert.CertPathValidatorException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ro.ubbcluj.cs.eventplannerclient.content.Event;
import ro.ubbcluj.cs.eventplannerclient.content.EventType;
import ro.ubbcluj.cs.eventplannerclient.content.User;

import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.AUTHOR;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.DESCRIPTION;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.ID;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.NAME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_DATE;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.START_TIME;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.TYPE;
import static ro.ubbcluj.cs.eventplannerclient.net.mapping.Api.Event.USERS;

public class EventReader implements ResourceReader<Event, JsonReader> {
    private static final String TAG = EventReader.class.getSimpleName();

    private ResourceReader<User, JsonReader> mUserReader;
    private ResourceReader<List<User>, JsonReader> mUsersReader;

    public EventReader() {
        mUserReader = new UserReader();
        mUsersReader = new ResourceListReader<>(mUserReader);
    }

    @Override
    public Event read(JsonReader jsonReader) throws IOException, JSONException, Exception {
        Event event = new Event();

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case (ID):
                    event.setId(jsonReader.nextLong());
                    break;
                case (NAME):
                    event.setName(jsonReader.nextString());
                    break;
                case (DESCRIPTION):
                    event.setDescription(jsonReader.nextString());
                    break;
                case (START_DATE):
                    jsonReader.beginArray();
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.set(Calendar.YEAR, jsonReader.nextInt());
                    calendar1.set(Calendar.MONTH, jsonReader.nextInt());
                    calendar1.set(Calendar.DAY_OF_MONTH, jsonReader.nextInt());
                    jsonReader.endArray();

                    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime());
                    event.setStartDate(startDate);
                    break;
                case (START_TIME):
                    jsonReader.beginArray();
                    Calendar calendar2 = Calendar.getInstance();
                    if (jsonReader.hasNext()) {
                        calendar2.set(Calendar.HOUR, jsonReader.nextInt());
                    } else {
                        calendar2.set(Calendar.HOUR, 0);
                    }

                    if (jsonReader.hasNext()) {
                        calendar2.set(Calendar.MINUTE, jsonReader.nextInt());
                    } else {
                        calendar2.set(Calendar.MINUTE, 0);
                    }

                    jsonReader.endArray();

                    String startTime = new SimpleDateFormat("HH:mm").format(calendar2.getTime());
                    event.setStartTime(startTime);
                    break;
                case (TYPE):
                    event.setType(EventType.valueOf(jsonReader.nextString()));
                    break;
                case (AUTHOR):
                    event.setAuthor(mUserReader.read(jsonReader));
                    break;
                case (USERS):
                    if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                        event.setUsers(mUsersReader.read(jsonReader));
                    } else {
                        //we receive null - it means we have an empty array
                        event.setUsers(new ArrayList<User>());
                        jsonReader.nextNull();
                    }
                    break;
                default:
                    jsonReader.skipValue();
                    Log.w(TAG, String.format("Event property '%s' is ignored.", name));
                    break;
            }
        }
        jsonReader.endObject();

        return event;
    }
}
