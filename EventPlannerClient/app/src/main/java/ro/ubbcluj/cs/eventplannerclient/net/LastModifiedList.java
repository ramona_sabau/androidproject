package ro.ubbcluj.cs.eventplannerclient.net;

import java.util.List;

public class LastModifiedList<E> {
  private String mLastModified;
  private List<E> mList;

  public LastModifiedList() {
  }

  public LastModifiedList(String lastModified, List<E> list) {
    mLastModified = lastModified;
    mList = list;
  }

  public String getLastModified() {
    return mLastModified;
  }

  public void setLastModified(String lastModified) {
    this.mLastModified = lastModified;
  }

  public List<E> getList() {
    return mList;
  }

  public void setList(List<E> list) {
    this.mList = list;
  }
}
